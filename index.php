<?php

require_once 'autoload/autoload.php';

$calculator = new \calculator\SimpleCalculator();

if (isset($_POST['equation'])){
    $calculator->calculate($_POST['equation']);
}

 $calculator->getCalculatorView();