<?php
/** @var $this \calculator\SimpleCalculator */
?>
<form method="post">
    <td><input type="text" name="equation" placeholder="equation" id="equation" value="<?= $_POST['equation']??$_POST['equation']?>"></td>
    <td><input type="text" name="result" placeholder="result" disabled value="<?= $this->result??$this->result?>"></td>
    <table>
        <tr>
            <td><button onclick="addToEquation(this)">1</button></td>
            <td><button onclick="addToEquation(this)">2</button></td>
            <td><button onclick="addToEquation(this)">3</button></td>
            <td><button onclick="addToEquation(this)">+</button></td>
        </tr>
        <tr>
            <td><button onclick="addToEquation(this)">4</button></td>
            <td><button onclick="addToEquation(this)">5</button></td>
            <td><button onclick="addToEquation(this)">6</button></td>
            <td><button onclick="addToEquation(this)">-</button></td>
        </tr>
        <tr>
            <td><button onclick="addToEquation(this)">7</button></td>
            <td><button onclick="addToEquation(this)">8</button></td>
            <td><button onclick="addToEquation(this)">9</button></td>
            <td><button onclick="addToEquation(this)">*</button></td>
        </tr>
        <tr>
            <td><button onclick="addToEquation(this)">0</button></td>
            <td><button onclick="addToEquation(this)">/</button></td>
            <td><button onclick="addToEquation(this)">sqrt</button></td>
            <td><button>Send</button></td>
            <td><button onclick="clearEquation()">Clear</button></td>
        </tr>
    </table>
</form>

<script>
    function addToEquation(button) {
        event.preventDefault();
        var equation = document.getElementById('equation');
        var value = button.innerText;
        equation.value = equation.value + value;
    }

    function clearEquation() {
        event.preventDefault();
        document.getElementById('equation').value = '';
    }
</script>