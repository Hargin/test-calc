<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/20/2022
 * Time: 7:39 AM
 */

namespace calculator\services;

/**
 * Interface Calculator
 * @package calculator\services
 */
interface Calculator
{

    public function getCalculatorView():string;

    public function calculate($equation):float;

}