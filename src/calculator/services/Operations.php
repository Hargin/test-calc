<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/20/2022
 * Time: 7:38 AM
 */

namespace calculator\services;


abstract class Operations
{
    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function plus($x,$y):float
    {
        return $x+$y;
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function minus($x,$y):float
    {
        return $x-$y;
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function multiply($x,$y):float
    {
        return $x*$y;
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function divide($x,$y):float
    {
        return $x/$y;
    }

    /**
     * @param float $x
     * @return float
     */
    protected function sqrt($x):float
    {
        return sqrt($x);
    }
}