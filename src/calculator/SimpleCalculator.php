<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/20/2022
 * Time: 7:58 AM
 */

namespace calculator;


use calculator\services\Operations;
use calculator\services\Calculator;

/**
 * Class SimpleCalculator
 * @package calculator
 *
 * @property float $x
 * @property float $y
 * @property string $operation
 * @property float $result
 */
class SimpleCalculator extends Operations implements Calculator
{
    protected $x;
    protected $y;
    protected $operation;
    protected $result;

    public function getCalculatorView():string
    {
        return require_once 'views/simple-calculator.php';
    }

    public function calculate($equation):float
    {
        $this->setOperands($equation);

        try{
            $this->result = $this->getResult();
        }catch (\Exception $e){
            echo $e->getMessage();
        }

        return round($this->result,6);
    }

    private function setOperands($equation):bool
    {
        preg_match_all('/\d+/',$equation,$arguments);
        preg_match('/\D+/',$equation,$operator);

        if(!isset($arguments[0])||!isset($operator[0])){
            return false;
        }

        $this->x = (float)$arguments[0][0];
        $this->y = isset($arguments[0][1])?(float)$arguments[0][1]:null;
        $this->operation = $operator[0];

        return true;
    }

    /**
     * @return float
     * @throws \Exception
     */
    private function getResult()
    {
        switch ($this->operation){
            case '+': return $this->plus($this->x,$this->y);
            case '-': return $this->minus($this->x,$this->y);
            case '*': return $this->multiply($this->x,$this->y);
            case '/': return $this->divide($this->x,$this->y);
            case 'sqrt': return $this->sqrt($this->x);
            default : throw new \Exception('Undefined operator!');
        }
    }
}