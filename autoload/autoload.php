<?php
require_once 'Autoloader.php';

$autoloader = new \autoload\Autoloader();

$autoloader->addNamespace('calculator\services','src\calculator\services');
$autoloader->addNamespace('calculator','src\calculator');
$autoloader->addNamespace('tests\calculator','tests\calculator');

$autoloader->loadClass('calculator\services\Operations');
$autoloader->loadClass('calculator\services\Calculator');
$autoloader->loadClass('calculator\SimpleCalculator');
$autoloader->loadClass('tests\calculator\SimpleCalculatorTest');