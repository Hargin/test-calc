<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/20/2022
 * Time: 7:58 AM
 */

namespace tests\calculator;


use calculator\services\Operations;
use calculator\services\Calculator;
use calculator\SimpleCalculator;
use PHPUnit\Framework\TestCase;

/**
 * Class SimpleCalculatorTest
 * @package test\calculator
 */
final class SimpleCalculatorTest extends TestCase
{

    public function testGetCalculatorView()
    {
        $this->assertTrue(file_exists( str_replace('\tests\calculator','',__DIR__).'/src/calculator/views/simple-calculator.php'));
    }

    public function testCalculate()
    {
        $calculator = new SimpleCalculator();
        $this->assertEquals(4,$calculator->calculate('2+2'));
        $this->assertEquals(2,$calculator->calculate('4-2'));
        $this->assertEquals(8,$calculator->calculate('4*2'));
        $this->assertEquals(2,$calculator->calculate('4/2'));
        $this->assertEquals(3,$calculator->calculate('9sqrt'));
    }

}